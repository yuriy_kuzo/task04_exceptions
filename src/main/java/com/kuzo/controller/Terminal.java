package com.kuzo.controller;

import com.kuzo.army.Army;
import com.kuzo.model.Battlefield;

import java.util.ArrayList;

public class Terminal implements Controller {
    private Battlefield battlefield;
    public Terminal() {
        battlefield = new Battlefield();
    }
    @Override
    public ArrayList<Army> getArmyList() {
        return battlefield.getArmyList();
    }


}
