package com.kuzo.controller;

import com.kuzo.army.Army;

import java.util.ArrayList;

public interface Controller {
    ArrayList<Army> getArmyList();

}
