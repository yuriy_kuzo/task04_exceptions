package com.kuzo.view;

@FunctionalInterface
public interface Printable {
    void print();
}
