package com.kuzo.view;

import com.kuzo.army.Army;
import com.kuzo.army.Droids;
import com.kuzo.controller.Controller;
import com.kuzo.controller.Terminal;
import com.kuzo.model.Battlefield;
import com.kuzo.model.Model;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView implements View {
    public static int number;
    public static String side;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public MyView() {
        controller = new Terminal();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print list of all droids");
        menu.put("2", "  2 - add droids");
        menu.put("E", "  E - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);

    }
    private void pressButton1() {
        for (Army army : controller.getArmyList()) {
            System.out.println(army);
        }
    }

    private void pressButton2() {
        System.out.println("Please input quantity of the droids you want to add");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        /*System.out.println("Please input side of the droids you want to add");
        String side = scanner.nextLine();*/
        Droids droid = new Droids(number);
        Battlefield.addDroid(droid);
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Select menu point");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) { }
        } while (!keyMenu.equals("E"));
    }
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}

