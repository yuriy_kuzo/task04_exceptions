package com.kuzo.view;

public interface View {
    void show();
}
