package com.kuzo.model;

import com.kuzo.army.Army;
import com.kuzo.army.Droids;
import com.kuzo.view.MyView;

import java.util.ArrayList;

public class Battlefield implements Model {
    private static ArrayList<Army> ArmyList;
    public Battlefield() {
        super();
        ArmyList = new ArrayList<Army>();
    }
    @Override
    public ArrayList<Army> getArmyList() {
        return ArmyList;
    }
    public static void addDroid(Droids droid) {
        ArmyList.add(droid);
    }
}
